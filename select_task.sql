/*1 БД «Комп. фірма». Знайти виробників ноутбуків. Вивести:
maker, type. Вихідні дані впорядкувати за зростанням за
стовпцем maker.*/
select maker,type 
from product
where type = "Laptop"
order by maker;

/*2 БД «Комп. фірма». Знайти номер моделі, об’єм пам’яті та
розміри екранів ноутбуків, ціна яких перевищує 1000 дол.
Вивести: model, ram, screen, price. Вихідні дані впорядкувати
за зростанням за стовпцем ram та за спаданням за стовпцем
price.*/
select model,ram,screen,price
from laptop
where price>1000
order by ram, price DESC;

/*3 БД «Комп. фірма». Знайдіть усі записи таблиці Printer для
кольорових принтерів. Вихідні дані впорядкувати за
спаданням за стовпцем price.*/
select *
from printer
where color = "y"
order by price DESC;

/*4 БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір
диску ПК, що мають CD-приводи зі швидкістю '12х' чи '24х' та
ціну меншу 600 дол. Вивести: model, speed, hd, cd, price.
Вихідні дані впорядкувати за спаданням за стовпцем speed.*/
select model, speed, hd, cd, price
from pc
where (cd = "12x" or cd = "24x") and price <= 600
order by speed DESC;

/*5 БД «Кораблі». Перерахувати назви головних кораблів (з
таблиці Ships). Вивести: name, class. Вихідні дані
впорядкувати за зростанням за стовпцем name.*/
select name,class
from ships
where class = name
order by name;

/*6 БД «Комп. фірма». Отримати інформацію про комп’ютери, що
мають частоту процесора не менше 500 МГц та ціну нижче 800
дол. Вихідні дані впорядкувати за спаданням за стовпцем price.*/
select *
from pc
where speed >=500 and price <=800
order by price desc;

/*7 БД «Комп. фірма». Отримати інформацію про всі принтери,
які не є матричними та коштують менше 300 дол. Вихідні дані
впорядкувати за спаданням за стовпцем type.*/
select *
from printer
where type!="matrix" and price<=300
order by type desc;

/*8 БД «Комп. фірма». Знайти модель та частоту процесора
комп’ютерів, що коштують від 400 до 600 дол. Вивести: model,
speed. Вихідні дані впорядкувати за зростанням за стовпцем
hd.*/
select model,speed
from pc
where price<=600 and price >=400
order by hd;

/*9 БД «Комп. фірма». Знайти модель, частоту процесора та об’єм
жорсткого диску для тих комп’ютерів, що комплектуються
накопичувачами 10 або 20 Мб та випускаються виробником
'A'. Вивести: model, speed, hd. Вихідні дані впорядкувати за
зростанням за стовпцем speed.*/
select pc.model, speed, hd , maker
from pc inner join product on pc.model = product.model
where (hd = 10 or hd = 20) and maker="A"
order by speed;

/*10 БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір
жорсткого диску для усіх ноутбуків, екран яких не менше 12
дюймів. Вивести: model, speed, hd, price. Вихідні дані
впорядкувати за спаданням за стовпцем price.*/
select model, speed, hd, price
from laptop
where screen >=12
order by price desc;

/*11 БД «Комп. фірма». Знайдіть номер моделі, тип та ціну для усіх
принтерів, вартість яких менше 300 дол. Вивести: model, type,
price. Вихідні дані впорядкувати за спаданням за стовпцем
type.*/
select model, type, price
from printer
where price <=300
order by type desc;

/*12 БД «Комп. фірма». Вивести моделі ноутбуків з кількістю RAM
рівною 64 Мб. Вивести: model, ram, price. Вихідні дані
впорядкувати за зростанням за стовпцем screen.*/
select model,ram,price
from laptop
where ram = 64
order by screen;

/*13 БД «Комп. фірма». Вивести моделі ПК з кількістю RAM
більшою за 64 Мб. Вивести: model, ram, price. Вихідні дані
впорядкувати за зростанням за стовпцем hd.*/
select model, ram, price
from pc
where ram>64
order by hd;

/*14 БД «Комп. фірма». Вивести моделі ПК зі швидкістю процесора
у межах від 500 до 750 МГц. Вивести: model, speed, price.
Вихідні дані впорядкувати за спаданням за стовпцем hd.*/
select model, speed, price
from pc
where speed>=500 and speed<=750
order by hd desc;

/*15 БД «Фірма прий. вторсировини». Вивести інформацію про
видачу грошей на суму понад 2000 грн. на пунктах прийому
таблиці Outcome_o. Вихідні дані впорядкувати за спаданням за
стовпцем date.*/
select *
from outcome_o
where outcome_o.out>=2000;

/*16 БД «Фірма прий. вторсировини». Вивести інформацію про
прийом грошей на суму у межах від 5 тис. до 10 тис. грн. на
пунктах прийому таблиці Income_o. Вихідні дані впорядкувати
за зростанням за стовпцем inc.*/
select *
from income_o
where inc>=5000 and inc<=10000
order by inc;

/*17 БД «Фірма прий. вторсировини». Вивести інформацію про
прийом грошей на пункті прийому No1 таблиці Income. Вихідні
дані впорядкувати за зростанням за стовпцем inc.*/
select * 
from income
where point = 1
order by inc;

/*18 БД «Фірма прий. вторсировини». Вивести інформацію про
видачу грошей на пункті прийому No2 таблиці Outcome.
Вихідні дані впорядкувати за зростанням за стовпцем out.*/
select *
from outcome
where point = 2
order by outcome.out;

/*19 БД «Кораблі». Вивести інформацію про усі класи кораблів для
країни 'Japan'. Вихідні дані впорядкувати за спаданням за
стовпцем type.*/
select *
from classes
where country = 'Japan'
order by type desc;

/*20 БД «Кораблі». Знайти всі кораблі, що були спущені на воду у
термін між 1920 та 1942 роками. Вивести: name, launched.
Вихідні дані впорядкувати за спаданням за стовпцем launched.*/
select name, launched
from ships
where launched>=1920 and launched <=1942
order by launched desc;

/*21 БД «Кораблі». Вивести усі кораблі, що брали участь у битві
'Guadalcanal' та не були потопленими. Вивести: ship, battle,
result. Вихідні дані впорядкувати за спаданям за стовпцем ship.*/
select ship, battle, result
from outcomes
where battle = 'Guadalcanal'
order by ship desc;

/*22 БД «Кораблі». Вивести усі потоплені кораблі. Вивести: ship,
battle, result. Вихідні дані впорядкувати за спаданням за
стовпцем ship.*/
select ship,battle,result
from outcomes
where result = 'sunk'
order by ship desc;

/*23 БД «Кораблі». Вивести назви класів кораблів з
водотоннажністю не меншою, аніж 40 тонн. Вивести: class,
displacement. Вихідні дані впорядкувати за зростанням за
стовпцем type.*/
select class, displacement
from classes
where displacement>=40000
order by type;

/*24 БД «Аеропорт». Знайдіть номера усіх рейсів, що бувають у
місті 'London'. Вивести: trip_no, town_from, town_to. Вихідні
дані впорядкувати за зростанням за стовпцем time_out.*/
select trip_no, town_from, town_to
from trip
where town_from = 'London' or town_to = 'London'
order by time_out;

/*25 БД «Аеропорт». Знайдіть номера усіх рейсів, на яких курсує
літак 'TU-134'. Вивести: trip_no, plane, town_from, town_to.
Вихідні дані впорядкувати за спаданням за стовпцем time_out.*/
select trip_no, plane, town_from, town_to
from trip
where plane = 'TU-134'
order by time_out desc;

/*26 БД «Аеропорт». Знайдіть номера усіх рейсів, на яких не курсує
літак 'IL-86'. Вивести: trip_no, plane, town_from, town_to. Вихідні
дані впорядкувати за зростанням за стовпцем plane.*/
select trip_no, plane, town_from, town_to
from trip
where plane !='IL-86'
order by plane;

/*27 БД «Аеропорт». Знайдіть номера усіх рейсів, що не бувають у
місті 'Rostov'. Вивести: trip_no, town_from, town_to. Вихідні дані
впорядкувати за зростанням за стовпцем plane.*/
select trip_no, town_from, town_to
from trip
where town_from !='Rostov' and town_to !='Rostov'
order by plane;

/*2.1 БД «Комп. фірма». Вивести усі моделі ПК, у номерах яких є
хоча б дві одинички.*/
select model
from pc
where model rlike '1.*1';

/*2.2 БД «Фірма прий. вторсировини». З таблиці Outcome вивести
усю інформацію за березень місяць.*/
select * 
from outcome
where month(date) = 3;

/*2.3 БД «Фірма прий. вторсировини». З таблиці Outcome_o вивести
усю інформацію за 14 число будь-якого місяця.*/
select * 
from outcome_o
where dayofmonth(date) = 14;

/*2.4 БД «Кораблі». З таблиці Ships вивести назви кораблів, що
починаються на 'W' та закінчуються літерою 'n'.*/
select name 
from ships
where name like 'n%';

/*2.5 БД «Кораблі». З таблиці Ships вивести назви кораблів, що
мають у своїй назві дві літери 'e'.*/
select name
from ships
where name rlike '^[^eE]*[eE][^eE]*[eE][^eE]*$';

/*2.6 БД «Кораблі». З таблиці Ships вивести назви кораблів та роки
їх спуску на воду, назва яких не закінчується на літеру 'a'.*/
select name, launched
from ships
where name not like '%a';

/*2.7 БД «Кораблі». Вивести назви битв, які складаються з двох слів
та друге слово не закінчується на літеру 'c'.*/
select *
from battles
where name rlike '^[a-zA-Z0-9_]+ [a-zA-Z0-9_]+$';

/*2.8 БД «Аеропорт». З таблиці Trip вивести інформацію про рейси,
що вилітають в інтервалі часу між 12 та 17 годинами включно.*/
select *
from trip
where hour(time_out)>=12 and hour(time_out)<=17;

/*2.9 БД «Аеропорт». З таблиці Trip вивести інформацію про рейси,
що прилітають в інтервалі часу між 17 та 23 годинами включно.*/
select *
from trip
where hour(time_in)>=17 and hour(time_in)<=23;

/*2.10 БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були
зайняті місця у першому ряду.*/
select date
from pass_in_trip
where place like '1%';

/*2.11 БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були
зайняті місця 'c' у будь-якому ряді.*/
select date
from pass_in_trip
where place like '%c';

/*2.12 БД «Аеропорт». Вивести прізвища пасажирів (друге слово у
стовпці name), що починаються на літеру 'C'.*/
select name
from passenger 
where name rlike ' [cC][a-zA-Z]+$';

/*2.13 БД «Аеропорт». Вивести прізвища пасажирів (друге слово у
стовпці name), що не починаються на літеру 'J'.*/
select name
from passenger 
where name rlike ' [^jJ][a-zA-Z]+$';

/*3.1 БД «Комп. фірма». Вкажіть виробника для тих ПК, що мають
жорсткий диск об’ємом не більше 8 Гбайт. Вивести: maker,
type, speed, hd.*/
select maker, type, speed, hd
from product inner join pc on product.model = pc.model
where hd<=8;

/*3.2 БД «Комп. фірма». Знайдіть виробників ПК з процесором не
менше 600 МГц. Вивести: maker.*/
select maker
from product inner join pc on product.model = pc.model
where speed>=600;

/*3.3 БД «Комп. фірма». Знайдіть виробників ноутбуків з
процесором не вище 500 МГц. Вивести: maker.*/
select maker
from product inner join laptop on product.model = laptop.model
where speed<=500;

/*3.4 БД «Комп. фірма». Знайдіть пари моделей ноутбуків, що
мають однакові об’єми жорстких дисків та RAM (таблиця
Laptop). У результаті кожна пара виводиться лише один раз.
Порядок виведення: модель з більшим номером, модель з
меншим номером, об’єм диску та RAM.*/
select distinct l1.model model_1, l2.model model_2, l1.hd, l2.ram
from laptop l1 join laptop l2 join product on l1.model = product.model
where l1.ram = l2.ram and l1.hd = l2.hd and l1.code!=l2.code and l1.model>=l2.model;

/*3.5 БД «Кораблі». Знайдіть країни, що мали класи як звичайних
бойових кораблів 'bb', так і класи крейсерів 'bc'. Вивести:
country, типи з класом 'bb', типи з класом 'bc'.*/
-- select distinct cl1.country
select cl1.country, cl1.class, cl1.type
from classes cl1 join classes cl2 on cl1.type != cl2.type 
where  cl1.country = cl2.country and cl1.class!=cl2.class
order by cl1.country;

/*3.6 БД «Комп. фірма». Знайдіть номер моделі та виробника ПК,
яка має ціну менше за 600 дол. Вивести: model, maker.*/
select product.model, maker
from product join pc on product.model = pc.model
where price<=600;

/*3.7 БД «Комп. фірма». Знайдіть номер моделі та виробника
прінтера, яка має ціну вищу за 300 дол. Вивести: model, maker.*/
select product.model, maker
from product join printer on product.model = printer.model
where price>=300;

/*3.8 БД «Комп. фірма». Виведіть виробника, номер моделі та ціну
кожного комп’ютера, що є у БД. Вивести: maker, model, price.*/
select maker, product.model, price
from product join pc on product.model = pc.model;

/*3.9 БД «Комп. фірма». Виведіть усі можливі моделі ПК, їх
виробників та ціну (якщо вона вказана). Вивести: maker,
model, price.*/
select product.model, maker, price 
from product join pc on product.model = pc.model
where price is not null
order by product.model;

/*3.10 БД «Комп. фірма». Виведіть виробника, тип, модель та частоту
процесора для ноутбуків, частота процесорів яких перевищує
600 МГц. Вивести: maker, type, model, speed.*/
select maker, type, product.model, speed
from product join laptop on product.model = laptop.model
where speed>=600;

/*3.11 БД «Кораблі». Для кораблів таблиці Ships вивести їх
водотоннажність.*/
select ships.class, name, displacement
from ships join classes on ships.class = classes.class;

/*3.12 БД «Кораблі». Для кораблів, що вціліли у битвах, вивести
назви та дати битв, у яких вони брали участь.*/
select battles.name battle, date , ship
from battles join outcomes on battles.name = outcomes.battle
where result = 'OK';

/*3.13 БД «Кораблі». Для кораблів таблиці Ships вивести країни,
яким вони належать.*/
select ships.name ship, country
from classes join ships on classes.class = ships.class;

/*3.14 БД «Аеропорт». Для рейсових літаків 'Boeing' вказати назви
компаній, яким вони належать.*/
select distinct name company
from trip join company on trip.ID_comp = company.ID_comp
where plane = 'Boeing';

/*3.15 БД «Аеропорт». Для пасажирів таблиці Passenger вивести
дати, коли вони користувалися послугами авіаліній.*/
select date, name
from passenger join pass_in_trip on passenger.ID_psg = pass_in_trip.ID_psg